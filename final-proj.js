// Project requirements and status:
// Interactive  DONE
// Event listeners  DONE (1, on form, submit event)
// Constructor function with a prototype  DONE
// Unit tests for all of the essential functions  NOT DONE  <-- started
// One or more timing functions  NOT DONE                   <--
// One or more fetch requests to a 3rd party API  DONE
// Sets, updates, or changes cookies or local storage  DONE
// Contains form fields, validates fields on update  DONE (1)

(function() {
  const API_BASE = 'https://api.openweathermap.org/data/2.5/forecast';
  const form = document.getElementsByTagName('form')[0];
  const zipInputEl = document.getElementById('zip');
  const showZipEl = document.getElementById('showZip');
  const apiErrorEl = document.getElementById('apiError');
  const resultDisplayEl = document.getElementById('resultDisplay');
  const clothingToAddEl = document.getElementById('clothingToAdd');
  let zipCode;
  const tomorrowEl = document.getElementById('isTomorrowsData');
  let isDataForTomorrow = 0;
  let hrEl;
  
  zipInputEl.value = localStorage.getItem('weatherZip');


  // Constructor function with a prototype -- private array with public getter/setter and clear
  function Gear(){
    this.extraClothing = [];
  }

  Gear.prototype.addClothing = function(clothing){
    this.extraClothing.push(clothing);
  }

  Gear.prototype.listClothing = function(){
    return this.extraClothing;
  }

  Gear.prototype.clearClothingList = function(){
    this.extraClothing = [];
  }

  let clothingToAdd = new Gear();
  
  
  // Form fields, validated on update -- validate the zip code field
  const validateZip = function(inputEl, submitEvent) {
    const inputErrorEl = inputEl.parentElement.querySelector('.error');
    const labelEl = inputEl.parentElement.querySelector('label');
    const zipRegex = /\d{5}/;

    if (!inputEl.value) {
      inputErrorEl.innerHTML = `Required`;
    } else if (!zipRegex.test(inputEl.value)) {
      inputErrorEl.innerHTML = `Not Valid`;
    } else {
      inputErrorEl.innerHTML = '';
      inputErrorEl.classList.remove('d-block');
      return true;
    }
    inputErrorEl.classList.add('d-block');
    submitEvent.preventDefault();
    return false;
  }

  const prepDisplayArea = function(weatherJson){
    // clear display area and error area
    resultDisplayEl.innerHTML = "";
    apiErrorEl.textContent = "";

    // enable exploring results at console
    // document.weatherJson = weatherJson;

    // display heading above data
    showZipEl.textContent = "Forecast for " + weatherJson.city.name + " " + zipCode + ":";

    // reveal display section
    resultDisplayEl.classList.add('visible');
    resultDisplayEl.classList.remove('hidden');

    // hide error section
    apiErrorEl.classList.add('hidden');
    apiErrorEl.classList.remove('visible');
  }

  let displayWeatherReadings = function(weatherJson){
    let dateSeconds;
    let date;
    let time;
    let lastDate = 0;
    hrEl = document.createElement('hr');
    let now = new Date();

    // Display the weather data for the first 10 3-hour data points, indicating the local time corresponding to each UTC data point
    for (i = 0; i < 10; i++){
      // get local date/time to show, from the reading
      dateSeconds = weatherJson.list[i].dt;
      date = new Date(dateSeconds * 1000);

      // get the temperature and weather description, from the reading
      temp = Math.round(weatherJson.list[i].main.temp);
      weathDesc = weatherJson.list[i].weather[0].description;

      // create a <p> tag that will display the date/time, temperature, and weather description
      var readingP = document.createElement('p');

      // convert military time to am/pm
      if (date.getHours() == 0){
        time = ' 12 a.m.';
      } else if (date.getHours() < 13){
        time = date.getHours() + ' a.m.';
      } else {
        time = (date.getHours() - 12) + ' p.m.';
      }

      readingP.textContent = date.getMonth()+1 + '/' + date.getDate() + '/' + date.getFullYear() + ' ' + time + ' -- ' +  temp + ' degrees, ' + weathDesc;

      // highlight if getHours() == 5 (5 am), 17 (5 pm), or 20 (8 pm)
      if ((date.getHours() == '5') || (date.getHours() == '17') || (date.getHours() == '20')){

        readingP.classList.add('highlight');

        // if much rain
        if (weatherJson.list[i].rain){
          if ((now.getDate() + isDataForTomorrow == date.getDate()) && (weatherJson.list[i].rain["3h"] > 1)){
            clothingToAdd.addClothing("thick shoes");
            clothingToAdd.addClothing("shoe covers");
          }
        }

        if (tomorrowEl.checked){
          isDataForTomorrow = 1;
        } else {
          isDataForTomorrow = 0;
        }

        // if any temp today > 50
        if ((now.getDate() + isDataForTomorrow == date.getDate()) && (temp > 50)){
          clothingToAdd.addClothing("tank top");
          clothingToAdd.addClothing("thin gloves");
        }
        // if any temp today < 40
        if ((now.getDate() + isDataForTomorrow == date.getDate()) && (temp < 40)){
          clothingToAdd.addClothing("scarf");
          clothingToAdd.addClothing("thick gloves");
        }
        // if any temp today < 35
        if ((now.getDate() + isDataForTomorrow == date.getDate()) && (temp < 35)){
          clothingToAdd.addClothing("windbreaker");
          clothingToAdd.addClothing("pants");
          clothingToAdd.addClothing("long socks");
        }
      }

      if (date.getDate() > lastDate){
        if (lastDate != 0){
          hrEl = document.createElement('hr');
          resultDisplayEl.append(hrEl);
        }
        lastDate = date.getDate();
      }

      // put the reading <p> tag into the display <div>
      resultDisplayEl.append(readingP);
    } // for
  }

  let displayClothingToAdd = function(){
    let h5El = document.createElement('h5');
    h5El.textContent = 'Clothing to Add:';
    resultDisplayEl.append(h5El);
    let clothesListEl = document.createElement('p');
    clothesListEl.setAttribute('id', 'clothes-display');
    clothesListEl.textContent = clothingToAdd.listClothing().join(', ');
    resultDisplayEl.append(clothesListEl);
  }

  // Event listener: listen for form submit and Enter key when on zip field
  form.addEventListener('submit', function(e) {

    if (validateZip(zipInputEl, e)){
      e.preventDefault();
    
      zipCode = zipInputEl.value;
      // clear zip input element
      zipInputEl.value = "";
      showZipEl.textContent = "Forecast for " + zipCode;
      clothingToAdd.clearClothingList();
      let pEl = document.getElementById('clothes-display');
      if (pEl){
        pEl.innerText = "";
      }

      // Sets local storage -- store the zip code entered, save for 6 months = 60 sec x 60 x 24 x 30 x 6 
      localStorage.setItem('weatherZip', `${zipCode}`);
      
      // Get the weather data
      const url = `${API_BASE}?zip=${zipCode}&units=imperial&appid=${API_KEY}`;
      
      // One or more fetch requests to a 3rd party API -- fetch day's hourly weather data from RESTful web service
      fetch(url)
      .then(function(response) {
        return response.json();
      })
      .then(function(weatherJson) {
        prepDisplayArea(weatherJson);
        displayWeatherReadings(weatherJson);
        displayClothingToAdd();
      })
      .catch(function(e){
        apiErrorEl.classList.add('visible');
        apiErrorEl.classList.remove('hidden');
        apiErrorEl.textContent = "Unable to get data for the specified zip code";
        resultDisplayEl.classList.add('hidden');
        resultDisplayEl.classList.remove('visible');
      });
    } // if (validate)
  }); // addEventListener('submit')
})(); // iffe
