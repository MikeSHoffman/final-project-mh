# Final Project - Michael Hoffman, JScript 200A, March 2019


## Setup

To use final-proj.html:

1. Get an API key from [Open Weather Map](https://openweathermap.org).

2. Create an apikey.js file in this directory, containing the following line of code.
   Replace `{{your_api_key}}` by your API key.

```javascript
const API_KEY = "{{your_api_key}}";
```
