describe("Validation tests", function() {

  it("when zip < 5 characters, should fail validation", function() {
//    expect(soccer.getTotalPoints('wwdl')).toEqual(7);
//    expect(soccer.getTotalPoints('wlldwddldw')).toEqual(13);
  });
  
  it("when zip is 5 characters, should pass validation", function() {
//    expect(soccer.getTotalPoints('wwdl')).toEqual(7);
//    expect(soccer.getTotalPoints('wlldwddldw')).toEqual(13);
  });
  
});


describe("Fetch tests", function() {
  const url = `https://api.openweathermap.org/data/2.5/forecast?zip=98103&units=imperial&appid=${API_KEY}`;

  
  it("known-good fetch-url should return some data", function() {
    
    fetch(url)
    .then(function(response) {
      return response.json();
    })
    .then(function(weatherJson) {

    })

    
//    spyOn(soccer, 'getTotalPoints');

//    soccer.orderTeams(sounders, galaxy);

//    expect(soccer.getTotalPoints).toHaveBeenCalled();
//    expect(soccer.getTotalPoints).toHaveBeenCalledTimes(2);
//    expect(soccer.getTotalPoints).toHaveBeenCalledWith('wwdl');
//    expect(soccer.getTotalPoints).toHaveBeenCalledWith('wlld');
  });

  
  it("bad fetch URL should return error from service", function() {
//    const standings = soccer.orderTeams(sounders, galaxy);
//    expect(standings).toContain('Sounders: 7');
//    expect(standings).toContain('Galaxy: 4');
  });

});
